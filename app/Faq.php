<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Faq extends Authenticatable
{

    use Notifiable;

    protected $table = "faq";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question', 'description'
    ];

    const post_rules = [
        'question' => ['required', 'string', 'max:255'],
        'description' => ['required', 'string', 'max:255'],
    ];

    const edit_rules = [
        'question' => ['required', 'string', 'max:255'],
        'description' => ['required', 'string', 'max:255'],
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
