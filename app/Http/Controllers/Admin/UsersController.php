<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.users')->with('aUser', User::where('role', null)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $oUser = User::find($id);

        return view('admin.editUser')->with('oUser', $oUser);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validation($request->all());
        $oUser = User::find($id);

        $oUser->fill($request->all());

        $oUser->save();

        if($oUser->save()){
            return Redirect::route('users.index')->with(['alert' => ['class' => 'success', 'message' => 'Gegevens zijn gewijzigd']]);
        }else{
            return Redirect::route('users.index')->with(['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $oUser = User::find($id);
        $oUser->delete();

        if($oUser->delete()){
            return Redirect::route('users.index')->with(['alert' => ['class' => 'success', 'message' => 'Gebruiker is verwijderd']]);
        }else{
            return Redirect::route('users.index')->with(['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']);
        }
    }

    protected function Validation(array $data)
    {
        return Validator::make($data, User::edit_rules)->validate();
    }
}
