<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Faq;
use App\Http\Controllers\Controller;
use App\RespondFaq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RespondFaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $faq_id = request('faq_id');

        return view('faq_respond.create')->with('faq_id', $faq_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validation($request->all());
        $oRespondFaq = new RespondFaq($request->all());

        $oRespondFaq->faq_id = $request->faq_id;
        $oRespondFaq->user_id = Auth::id();
        $oRespondFaq->date = date('Y-m-d');

        if($oRespondFaq->save()){
            return Redirect::route('faq.index')->with(['alert' => ['class' => 'success', 'message' => 'Uw antwoord is ingevoerd']]);
        }else{
            return Redirect::route('faq.index')->with(['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $oRespondFaq = RespondFaq::find($id);

        return view('faq_respond.edit')->with('oRespondFaq', $oRespondFaq);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validation($request->all());
        $oUser = RespondFaq::find($id);

        $oUser->fill($request->all());

        $oUser->save();

        if($oUser->save()){
            return Redirect::route('faq.index')->with(['alert' => ['class' => 'success', 'message' => 'Gegevens zijn gewijzigd']]);
        }else{
            return Redirect::route('faq.index')->with(['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $oRespondFaq = RespondFaq::find($id);
        $oRespondFaq->delete();

        if($oRespondFaq->delete()){
            return Redirect::route('faq.index')->with(['alert' => ['class' => 'success', 'message' => 'Antwoord is verwijderd']]);
        }else{
            return Redirect::route('faq.index')->with(['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function Validation(array $data)
    {
        return Validator::make($data, RespondFaq::post_rules);

    }
}
