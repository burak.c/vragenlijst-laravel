<?php

namespace App\Http\Controllers\Faq;

use App\Http\Controllers\Controller;
use App\RespondFaq;
use App\User;
use Illuminate\Http\Request;
use App\Faq;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('faq.index')->with('aQuestion', Faq::where('visible', '0')->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hiddenIndex()
    {
        return view('faq.hidden')->with('aQuestion', Faq::where('visible', '1')->get());
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validation($request->all());
        $oFaq = new Faq($request->all());

        $oFaq->user_id = Auth::id();
        $oFaq->date = date('Y-m-d');
        $oFaq->visible = '0';

        if($oFaq->save()){
            return Redirect::route('faq.index')->with(['alert' => ['class' => 'success', 'message' => 'Uw vraag is ingediend']]);
        }else{
            return Redirect::route('faq.index')->with(['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $oFaq = Faq::find($id);
        $aAnswer = RespondFaq::where('faq_id', $id)->get();

        return view('faq.show')->with(['oFaq' => $oFaq, 'aAnswer' => $aAnswer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $oFaq = Faq::find($id);

        return view('faq.edit')->with('oFaq', $oFaq);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validation($request->all());
        $oFaq = Faq::find($id);

        $oFaq->fill($request->all());

        $oFaq->save();

        if($oFaq->save()){
            return Redirect::route('faq.index')->with(['alert' => ['class' => 'success', 'message' => 'Vraag is gewijzigd']]);
        }else{
            return Redirect::route('faq.index')->with(['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function hidden($id)
    {
        $oFaq = Faq::find($id);
        $oFaq->visible = !$oFaq->visible;

        $oFaq->save();

        if($oFaq->save()){
            return Redirect::route('faq.index')->with(['alert' => ['class' => 'success', 'message' => 'Vraag is verplaatst']]);
        }else{
            return Redirect::route('faq.index')->with(['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function Validation(array $data)
    {
        return Validator::make($data, Faq::post_rules);

    }
}
