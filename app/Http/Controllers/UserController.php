<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return array
     */
    protected function Validation(array $data)
    {
        return Validator::make($data, User::edit_rules)->validate();
    }

    public function profile()
    {
        return view('users.profile')->with('oUser', Auth::user());
    }

    public function edit()
    {
        return view('users.edit')->with('oUser', Auth::user());
    }

    public function remove()
    {
        return view('users.delete');
    }

    public function delete(){
        $oUser = Auth::user();

        if($oUser->delete()){
            Auth::logout();
            return Redirect::route('auth.login')->with(['alert' => ['class' => 'success', 'message' => 'Account verwijderd']]);
        }else{
            return Redirect::route('home')->with(['alert' => ['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']]);
        }
    }

    public function update(Request $request){
        $this->validation($request->all());
        $oUser = Auth::user();

        $oUser->fill($request->all());

        $oUser->save();

        if($oUser->save()){
            return Redirect::route('home')->with(['alert' => ['class' => 'success', 'message' => 'Gegevens zijn geupdate']]);
        }else{
            return Redirect::route('home')->with(['alert' => ['class' => 'danger', 'message' => 'Er is iets niet goed gegaan']]);
        }

    }
}
