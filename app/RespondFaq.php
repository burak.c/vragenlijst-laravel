<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class RespondFaq extends Authenticatable
{

    use Notifiable;

    protected $table = "faq_respond";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'answer'
    ];

    const post_rules = [
        'answer' => ['required', 'string', 'max:255'],
    ];

    const edit_rules = [
        'answer' => ['required', 'string', 'max:255'],
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function faq()
    {
        return $this->belongsTo('App\Faq');
    }
}
