@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create FAQ answer') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('faq_respond.store') }}">
                            @csrf
                            <input id="faq_id" type="text" class="form-control d-none" name="faq_id" value="{{ $faq_id }}">
                            <div class="form-group row">
                                <label for="answer" class="col-md-4 col-form-label text-md-right">{{ __('Answer') }}</label>
                                <div class="col-md-6">
                                    <input id="answer" type="text" class="form-control @error('answer') is-invalid @enderror" name="answer" value="{{ old('answer') }}">
                                    @error('answer')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create FAQ response') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
