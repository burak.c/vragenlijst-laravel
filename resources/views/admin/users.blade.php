@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Users') }}</div>

                    <div class="card-body">

                            @csrf
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">{{ __('Firstname') }}</th>
                                        <th scope="col">{{ __('Lastname') }}</th>
                                        <th scope="col">{{ __('Email') }}</th>
                                        <th scope="col">{{ __('City') }}</th>
                                        <th scope="col">{{ __('Zipcode') }}</th>
                                        <th scope="col">{{ __('Date of birth') }}</th>
                                        <th scope="col" width="20"></th>
                                        <th scope="col" width="20"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($aUser as $oUser)
                                    <tr>
                                        <td>{{ $oUser->id }}</td>
                                        <td>{{ $oUser->firstname }}</td>
                                        <td>{{ $oUser->lastname }}</td>
                                        <td>{{ $oUser->email }}</td>
                                        <td>{{ $oUser->city }}</td>
                                        <td>{{ $oUser->zipcode }}</td>
                                        <td>{{ $oUser->date_of_birth }}</td>
                                        <td>
                                            <a class="btn btn-primary text-white" href="{{ route('users.edit', ['user' => $oUser]) }}"><i class="far fa-edit"></i></a>
                                        </td>
                                        <td>
                                            {{ Form::open([ 'method' => 'delete', 'route' => [ 'users.destroy', $oUser->id] ])}}
                                            {{ Form::button('<i class="fa fa-trash-alt"></i>',['type' => 'submit', 'class' => 'btn btn-danger']) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
