@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Delete user') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('deleteUser', $oUser->id) }}">
                            @method('DELETE')
                            @csrf
                            <p class="text-center"><b>Weet je zeker dat je {{ $oUser->firstname }} {{ $oUser->lastname }}'s account wilt verwijderen?</b></p>

                            <div class="row">
                                <div class="col text-center">
                                    <button type="submit" class="btn btn-danger">
                                        {{ __('Delete permanently') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
