@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('FAQ') }}
                        <a class="btn btn-primary float-right text-white"  href="{{ route('faq.index')}}">Terug naar de FAQ</a>
                    </div>

                    <div class="card-body">

                        @csrf
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Question') }}</th>
                                <th scope="col">{{ __('Date') }}</th>
                                <th scope="col">{{ __('Actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($aQuestion as $oQuestion)
                                <tr class='clickable-row' data-href="{{ route('faq.show', $oQuestion->id)}}">
                                    <td>{{ $oQuestion->id }}</td>
                                    <td>{{ $oQuestion->question }}</td>
                                    <td>{{ $oQuestion->date }}</td>
                                    <td>
                                        {{ Form::open([ 'method' => 'patch', 'action' => [ 'Faq\FaqController@hidden', $oQuestion->id] ])}}
                                        {{ Form::button('<i class="fas fa-eye"></i>',['type' => 'submit', 'class' => 'btn btn-primary replace']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
