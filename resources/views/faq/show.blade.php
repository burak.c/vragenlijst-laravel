@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Question') }}</div>
                    <div class="card-body">
                            <div class="form-group ">
                                <h2><b>{{ $oFaq->question }}?</b></h2>
                            </div>
                            <div class="form-group ">
                                <label>{{ $oFaq->description }}</label>
                            </div>
                    </div>
                </div>
                @if(Auth::user()->isRole() == 'admin')
                <a class="btn btn-primary text-white mt-4" href="{{ route('faq_respond.create', ['faq_id' => $oFaq->id]) }}">Voeg een antwoord toe</a>
                @endif
                @if(!$aAnswer->isEmpty())
                <div class="card mt-2">
                    <div class="card-header">{{ __('Answer') }}</div>
                    <div class="card-body">
                        @foreach($aAnswer as $answers)
                            <div class="clearfix mt-3">
                                <div class="float-left mt-2">{{ $answers->answer }}</div>
                                @if(Auth::user()->isRole() == 'admin')
                                <div class="float-right">
                                    {{ Form::open([ 'method' => 'delete', 'route' => [ 'faq_respond.destroy', $answers->id] ])}}
                                    {{ Form::button('<i class="fa fa-trash-alt"></i>',['type' => 'submit', 'class' => 'btn btn-danger']) }}
                                    {{ Form::close() }}
                                </div>
                                <div class="float-right">
                                    <a class="btn btn-primary text-white" href="{{ route('faq_respond.edit', ['faq_respond' => $answers]) }}"><i class="far fa-edit"></i></a>
                                </div>
                                @endif
                                <div class="float-right mr-3 mt-2"><b>{{ $answers->date }} door: {{ $answers->user->firstname }} {{ $answers->user->lastname }}</b></div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection
