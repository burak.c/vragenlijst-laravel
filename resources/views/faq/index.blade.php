@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('FAQ') }}
                        @if(Auth::user()->isRole()== 'admin')
                            <a class="btn btn-danger float-right text-white" href="{{ route('hidden')}}">Verplaatste vragen</a>
                        @endif
                        <a class="btn btn-primary float-right text-white mr-3"  href="{{ route('faq.create')}}">Voeg een vraag toe</a>
                    </div>

                    <div class="card-body">

                        @csrf
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Question') }}</th>
                                <th scope="col">{{ __('Date') }}</th>
                                <th scope="col">{{ __('Geplaatst door') }}</th>
                                @if(Auth::user()->isRole()== 'admin')
                                    <th scope="col" width="20"></th>
                                    <th scope="col" width="20"></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($aQuestion as $oQuestion)
                                <tr class='clickable-row' data-href="{{ route('faq.show', $oQuestion->id)}}">
                                    <td>{{ $oQuestion->id }}</td>
                                    <td>{{ $oQuestion->question }}</td>
                                    <td>{{ $oQuestion->date }}</td>
                                    <td>{{ $oQuestion->user->email }}</td>
                                    @if(Auth::user()->isRole()== 'admin')
                                        <td>
                                            <a class="btn btn-primary text-white" href="{{ route('faq.edit', ['faq' => $oQuestion]) }}"><i class="far fa-edit"></i></a>
                                        </td>
                                        <td>
                                            {{ Form::open([ 'method' => 'patch', 'action' => [ 'Faq\FaqController@hidden', $oQuestion->id] ])}}
                                            {{ Form::button('<i class="fa fa-eye-slash"></i>',['type' => 'submit', 'class' => 'btn btn-danger replace']) }}
                                            {{ Form::close() }}
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
