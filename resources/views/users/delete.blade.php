@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Delete') }}</div>

                    <div class="card-body">
                        <p class="text-center"><b>Weet je zeker dat je jouw account wilt verwijderen?</b></p>
                        <form method="POST" action="{{ route('delete') }}">
                            @csrf
                            <div class="row">
                                <div class="col text-center">
                                    <button type="submit" class="btn btn-danger">
                                        {{ __('Delete permanently') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
