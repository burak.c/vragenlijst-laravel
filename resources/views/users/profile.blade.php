@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Profile') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('profile') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="firstname" class="col-md-4 text-md-right"><b>{{ __('Firstname') }}:</b></label>
                                {{ $oUser->firstname }}
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4  text-md-right"><b>{{ __('Lastname') }}:</b></label>
                                {{ $oUser->lastname }}
                            </div>

                            <div class="form-group row">
                                <label for="date_of_birth" class="col-md-4  text-md-right"><b>{{ __('Email') }}:</b></label>
                                {{ $oUser->email }}
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4  text-md-right"><b>{{ __('City') }}:</b></label>
                                {{ $oUser->city }}
                            </div>

                            <div class="form-group row">
                                <label for="zipcode" class="col-md-4  text-md-right"><b>{{ __('Zipcode') }}:</b></label>
                                {{ $oUser->zipcode }}
                            </div>

                            <div class="form-group row">
                                <label for="date_of_birth" class="col-md-4  text-md-right"><b>{{ __('Date of birth') }}:</b></label>
                                {{ $oUser->date_of_birth }}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
