@extends('layouts.app')

@section('content')
<header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h1 class="mb-5">Moet ik naar de optometrist?</h1>
            </div>
        </div>
    </div>
</header>
<section class="features-icons bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <i class="fas fa-info-circle text-primary fa-6x icon"></i>
                    <h3>Algemene informatie</h3>
                    <p class="lead mb-0">This theme will look great on any device, no matter the size!</p>
                    <a class="btn btn-primary text-white mt-3 btn-lg">Kom meer te weten!</a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <i class="fas fa-eye text-primary fa-6x icon"></i>
                    <h3>Ik heb een zorgvraag</h3>
                    <p class="lead mb-0">This theme will look great on any device, no matter the size!</p>
                    <a class="btn btn-primary text-white mt-3 btn-lg">Kom meer te weten!</a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <i class="fas fa-question-circle text-primary fa-6x icon"></i>
                    <h3>FAQ</h3>
                    <p class="lead mb-0">This theme will look great on any device, no matter the size!</p>
                    <a class="btn btn-primary text-white mt-3 btn-lg" href="{{ route('faq.index') }}">Kom meer te weten!</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
