<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Faq;
use App\User;
use Faker\Generator as Faker;

$factory->define(Faq::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random()->id,
        'question' => $faker->sentence(10),
        'description' => $faker->sentence(25),
        'visible' => 0,
        'date' => now(),

    ];
});
