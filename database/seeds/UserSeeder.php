<?php

use Illuminate\Database\Seeder;
use \App\User;
use \Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oUser = new User();

        $oUser->firstname = 'Burak';
        $oUser->lastname = 'Cengioglu';
        $oUser->city = 'Doevoe';
        $oUser->zipcode = '1115GJ';
        $oUser->email = 'burakcengioglu@hotmail.com';
        $oUser->date_of_birth = date("Y-m-d", strtotime("1999-05-15"));
        $oUser->password = Hash::make('123456789');
        $oUser->role = User::admin;

        $oUser->save();
    }
}
