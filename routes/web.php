<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Authentication Routes...
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login.user');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::middleware('auth')->group(function() {

    Route::get('/home', 'HomeController@index')->name('home');

    // Edit user Routes...
    Route::post('edit', 'UserController@update')->name('update');
    Route::get('edit', 'UserController@edit')->name('edit');

    // User Routes...
    Route::get('profile', 'UserController@profile')->name('profile');

    // Delete user routes...
    Route::post('delete', 'UserController@delete')->name('delete');
    Route::get('delete', 'UserController@remove')->name('remove');

    Route::resource('faq', 'Faq\FaqController');
    Route::patch('faq{id}', 'Faq\FaqController@hidden');
    Route::get('hidden', 'Faq\FaqController@hiddenIndex')->name('hidden');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth' => 'admin'], 'namespace' => 'Admin'], function(){
    Route::resource('users', 'UsersController');

    Route::get('faq_respond{faq_id}', 'Faq\RespondFaqController@create')->name('faq_respond.create');
    Route::post('faq_respond', 'Faq\RespondFaqController@store')->name('faq_respond.store');
    Route::delete('faq_respond{id}', 'Faq\RespondFaqController@destroy')->name('faq_respond.destroy');
    Route::post('faq_respond/edit{faq_respond}', 'Faq\RespondFaqController@update')->name('faq_respond.update');
    Route::get('faq_respond/edit{faq_respond}', 'Faq\RespondFaqController@edit')->name('faq_respond.edit');
});


